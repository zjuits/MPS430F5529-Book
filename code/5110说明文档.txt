  LcdWrite(LCD_CMD, 0x21);  // LCD Extended Commands.   LCD进入到扩展命令模式
  LcdWrite(LCD_CMD, 0xB1);  // Set LCD Vop (Contrast). //B1   如果屏幕黑，把这个值改小，如果屏幕白，改大这个值
  LcdWrite(LCD_CMD, 0x04);  // Set Temp coefficent. //0x04   温度系数   04 05 06 07 智能是这四个值
  LcdWrite(LCD_CMD, 0x13);  // LCD bias mode 1:48. //0x13 Modify Back Color  改对比度，首位必须为1

  LcdWrite(LCD_C, 0x20);   //普通命令模式
  LcdWrite(LCD_C, 0x0C);   //进行显示
#include <Wire.h>

int TMP75_Address = 0x48;

int decPlaces = 1;
int numOfBytes = 2;
byte configReg = 0x01;      // Address of Configuration Register
byte bitConv = B01100000;   // Set to 12 bit conversion
byte rdWr = 0x01;           // Set to read write
byte rdOnly = 0x00;         // Set to Read

float temperature;

String SerialDegC;

#define PWM_Port P2_5

void setup() {
  // put your setup code here, to run once:
  
        pinMode(PWM_Port,OUTPUT);
        analogWrite(PWM_Port, 200);
	Serial.begin(9600);
	Wire.begin();
	Wire.beginTransmission(TMP75_Address);
	Wire.write(configReg);
	Wire.write(bitConv);                         // Set the temperature resolution
	Wire.endTransmission();                      // Stop transmitting
	Wire.beginTransmission(TMP75_Address);       // Address the TMP75 sensor
	Wire.write(rdOnly);                          // Address the Temperature register
	Wire.endTransmission();                      // Stop transmitting
	SerialDegC += " ";           // Setup a Degrees C Serial symbol
	SerialDegC += "C ";
}

void loop() {
  // put your main code here, to run repeatedly: 
  float temp = readTemp();             // Read the temperature now
  Serial.print(temp,decPlaces);        // Temperature value to 1 Decimal place no newline to serial
  Serial.println(SerialDegC);          // Degree symbol after the temperature value with newline
  delay(500);
}

float readTemp(){
  // Now take a Temerature Reading
  Wire.requestFrom(TMP75_Address,numOfBytes);  // Address the TMP75 and set number of bytes to receive
  byte MostSigByte = Wire.read();              // Read the first byte this is the MSB
  byte LeastSigByte = Wire.read();             // Now Read the second byte this is the LSB

  // Being a 12 bit integer use 2's compliment for negative temperature values
  int TempSum = (((MostSigByte << 8) | LeastSigByte) >> 4);
  // From Datasheet the TMP75 has a quantisation value of 0.0625 degreesC per bit
  float temp = (TempSum*0.0625);
  //Serial.println(MostSigByte, BIN);   // Uncomment for debug of binary data from Sensor
  //Serial.println(LeastSigByte, BIN);  // Uncomment for debug  of Binary data from Sensor
  return temp;                           // Return the temperature value
}

// Include application, user and local libraries
#include "Energia.h"
#include "LCD_5110.h"

// Variables
//uint8_t pinChipSelect, uint8_t pinSerialClock, uint8_t pinSerialData, uint8_t pinDataCommand, uint8_t pinReset, uint8_t pinBacklight, uint8_t pinPushButton
#define PIN_SCE   P2_0
#define PIN_RESET P2_2
#define PIN_DC    P2_1
#define PIN_SDIN  P1_7
#define PIN_SCLK  P1_5
#define PIN_BACK  P1_0

LCD_5110 myScreen(PIN_SCE,PIN_SCLK,PIN_SDIN,PIN_DC,PIN_RESET,PIN_BACK,PUSH2);
boolean	backlight = true;
uint8_t k = 0;


// Add setup code
void setup() {
    myScreen.begin();
    
    myScreen.setBacklight(backlight);
    myScreen.text(0, 0, "Hello!");
    
    delay(1000);
    //  myScreen.clear();
    myScreen.text(0, 5, "Light off");
}


// Add loop code
void loop() {
    if (myScreen.getButton()) {
        backlight = (backlight==0);
        myScreen.setFont(0);
        myScreen.text(0, 5, backlight ? "Light on " : "Light off");
        myScreen.setBacklight(backlight);
    }
    
    myScreen.setFont(1);
    if (k==0)   myScreen.text(0, 2, " MSP430");
    else if (k==8)   myScreen.text(0, 2, "  LM4F  ");
    
    myScreen.setFont(0);
    for (uint8_t i=0; i<14; i++) myScreen.text(i, 4, (i==k) ? "*" : " ");
    k++;
    k %= 14;
    
    delay(200);
}

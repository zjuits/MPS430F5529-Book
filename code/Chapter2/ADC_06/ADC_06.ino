int sensorPin = P1_4;    
int sensorValue = 0; 

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("send A/D data");
}

void loop()
{
    sensorValue = analogRead(sensorPin);  
    Serial.println(sensorValue);
    delay(500);
}
